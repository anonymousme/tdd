package primefactors

import spock.lang.Specification

class PrimeFactorsSpec extends Specification{
    def "api to t test nothing"(){
        expect: "nothing" == "nothing"
    }

    def "API to test prime factors of given integer"(){
        expect: "factorsOf(n) will return list with n's prime factors"
            PrimeFactors.factorsOf(n) == lst
        where:
        n                   ||          lst
        1                   ||       []
        2                   ||       [2]
        3                   ||       [3]
        4                   ||       [2,2]
        5                   ||       [5]
        6                   ||       [2,3]
        7                   ||       [7]
        8                   ||       [2,2,2]
        9                   ||       [3,3]
        2*2*3*3*7*11*11*13  ||       [2,2,3,3,7,11,11,13]
        36                  ||       [2,2,3,3]


    }


}
